##Installing the locale switcher

To install the locale switcher, we're essentially following the [Shopify API Getting Started Guide](https://help.shopify.com/api/getting-started) for private apps.

First, we'll grant access to the private app:

1. From your Shopify Admin, go to _Apps_.
1. Click _Manage private apps_ (it's a small link at the bottom).
1. Click _Create new private app_.
1. Fill in the _Private app name_ as "SmartHalo Locale Switcher".
1. In the _Admin API_ section, change everything to "No access" except for Script tags, which you'll set to "Read and write".
1. Click _Save_.

Your store will now create an API key and password.  Note these for the next step (installing the app):

1. Open up a Terminal window on your Mac.
1. Run the following `curl -H "Content-type: application/json" -X POST -d '{ "script_tag": { "event": "onload", "src": "https://bitbucket.org/smart_halo/public_files/downloads/locale-switcher.js" } }' 'https://<API KEY>:<PASSWORD>@<YOUR SHOP URL>/admin/script_tags.json'`

Open up your store and you'll see the locale switcher at the top right.

##Removing the local switcher

These instructions assume you have already installed via the instructions above.

1. Get the id of the script to delete by running this command: `curl -H "Content-type: application/json" -X GET 'https://<API KEY>:<PASSWORD>@<YOUR SHOP URL>/admin/script_tags.json'`
1. Delete the script by running this command: `curl -H "Content-type: application/json" -X PUT -d '{ "script_tag": { "event": "onload", "src": "https://bitbucket.org/smart_halo/public_files/downloads/locale-switcher.js" } }' 'https://<API KEY>:<PASSWORD>@<YOUR SHOP URL>/admin/script_tags/<SCRIPT ID>.json' `