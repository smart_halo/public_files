(function(){
	
	//"constants"
	const parentElement = document.getElementById("shopify-section-header")
	const localeLinks = Array(
		["USA/International - EN", "smarthalo-test.myshopify.com"],
		["Canada - EN", "ca.smarthalo.bike"],
		["Canada - FR", "ca.smarthalo.bike?lang=fr"],
		["EU - EN", "eu.smarthalo.bike"],
		["UK - EN", "uk.smarthalo.bike"]
	);

	const styleEl = document.createElement('style');
	styleEl.innerHTML = " \
		#shopify-section-header { \
			z-index: 100;\
		} \
		\
		#PageContainer { \
			z-index: 1; \
		} \
		\
		.smarthalo-locale-switcher { \
			display: inline-block; \
			width: auto; \
			height: 1.5em; \
			border: solid 1px #000; \
			position: absolute; \
			top: 0px; \
			right: 0px; \
			z-index: 2; \
		} \
		\
		ol.smarthalo-locale-switcher-list { \
			position: absolute; \
			top: 1.5em; \
			background: #fff; \
			width: 100%; \
			display: none; \
		} \
		\
		.smarthalo-locale-switcher:hover ol.smarthalo-locale-switcher-list { \
			display: block; \
		} \
		ol.smarthalo-locale-switcher-list li a { \
			display: block; \
			width: 100%; \
			height: 100% \
		} \
	"; //end of styleEl.innerHTML*/


	//create the elements we will insert
	var localeSwitcher = document.createElement("div");
	localeSwitcher.className = "smarthalo-locale-switcher";
    
	var curLocaleEl = document.createElement("div");
	curLocaleEl.className = "smarthalo-locale-switcher-curLocale";
	localeSwitcher.appendChild(curLocaleEl);

    var localeList = document.createElement("ol");
    localeList.className = "smarthalo-locale-switcher-list";
    localeSwitcher.appendChild(localeList);

    var curLocaleText = localeLinks[0][0]; //default current locale as the first in the list

    for( var i in localeLinks ) {
    	var linkInfo = localeLinks[i]

    	if( window.location.href.search(linkInfo[1]) > 0 ) {
    		curLocaleText = linkInfo[0]
    		continue;  //don't add a link if it's the current locale
    	} 
    	
    	var newListItem = document.createElement("li")
    	newListItem.innerHTML = "<a href='https://"+linkInfo[1]+"'>"+linkInfo[0]+"</a>"
    	localeList.appendChild(newListItem)
    }

    curLocaleEl.innerHTML = curLocaleText;

    //add to the page
    parentElement.appendChild(styleEl);
    parentElement.appendChild(localeSwitcher);

 })();